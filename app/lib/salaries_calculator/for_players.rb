module SalariesCalculator
  class ForPlayers
    def initialize(args)
      @players_json = args[:jugadores]
    end

    def calculate
      return [] if @players_json.blank?
      
      players_with_calculated_salaries
    end

    private

    def players_with_calculated_salaries
      build_teams.each do |team|
        team.players.each { |player| player.full_salary = player.calculate_full_salary(team.bonus_percentage) }
      end
      
      players_full_list(build_teams)
    end

    def build_teams
      @build_teams ||= group_by_teams.collect { |team_name, players_json| 
        Team.new({
          name: team_name,
          players: players_initialize(players_json)
        }) 
      }.compact
    end

    def group_by_teams
      @group_by_teams ||= @players_json.group_by{|p| p['equipo'] }
    end

    def players_initialize(hash_of_players={})
      Services::GetPlayersWithStruct.new(hash_of_players).call
    end

    def players_full_list(team_list)
      @players_full_list ||= team_list.collect { |team| team.players }.flatten
    end
  end
end
