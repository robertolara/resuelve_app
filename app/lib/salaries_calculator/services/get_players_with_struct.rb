module SalariesCalculator
  module Services
    class GetPlayersWithStruct
      def initialize(args=[])
        @players_hash = args
      end

      def call
        player_list = @players_hash.each_with_object([]) do |player_json, array_players|
          array_players << Player.new(player_json)
        end
      end
    end
  end
end