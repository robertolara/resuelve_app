class Team
  attr_reader :name,
    :players

  def initialize(params)
    @name = params[:name]
    @players = params[:players].try(:to_a) || []
  end

  def goals_scored
    @goals_scored ||= players.sum {|player| player.goals }
  end

  def goals_needed
    @goals_needed ||= players.sum {|player| Level::SOCORE_BY_TYPE[player.level]}
  end

  def bonus_percentage
    total_percentage = ((goals_scored.to_f * 100.to_f) / goals_needed.to_f)
    return 100.to_f if total_percentage > 100

    total_percentage
  end
end