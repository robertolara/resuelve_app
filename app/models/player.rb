class Player
  attr_reader :name,
    :level,
    :goals,
    :base_salary,
    :bonus,
    :team

  attr_accessor :full_salary

  def initialize(params)
    @name = params['nombre']
    @level = params['nivel']
    @goals = params['goles'].try(:to_i) || 0
    @base_salary = params['sueldo'].to_f
    @bonus = params['bono'].to_f
    @full_salary = params['sueldo_completo']
    @team = params['equipo']
  end

  def bonus_percentage
    total_percentage = (goals.to_f * 100.to_f) / Level::SOCORE_BY_TYPE[level].to_f
    return 100.to_f if total_percentage > 100

    total_percentage
  end

  def calculate_full_salary(team_percentage=0.0)
    total_percentage = (bonus_percentage + team_percentage) / 2
    total_amount_by_bonus = total_percentage * bonus / 100.0

    (total_amount_by_bonus + base_salary).truncate(2)
  end
end
