module Api::V1
  class PlayersController < ApplicationController
    def salaries
      players = SalariesCalculator::ForPlayers.new(player_params).calculate
      status = players.blank? ? :unprocessable_entity : :ok

      render json: PlayersRepresenter.new(players).as_json, status: status
    end

    private

    def player_params
      params.permit(jugadores: [:nombre, :nivel, :goles, :sueldo, :bono, :sueldo_completo, :equipo])
    end 
  end
end
