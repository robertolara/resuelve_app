class PlayersRepresenter
  attr_reader :players

  def initialize(players=[])
    @players = players
  end

  def as_json
    {
      jugadores: players_mapping
    }
  end

  private

  def players_mapping
    @players_mapping ||= players.map do |player|
      {
        nombre: player.name,
        nivel: player.level,
        goles: player.goals,
        sueldo: player.base_salary,
        bono: player.bonus,
        sueldo_completo: player.full_salary,
        equipo: player.team
      }
    end
  end
end
