#  Prueba Jugadores de Futbol

Aplicación para calcular el sueldo de los jugadores de futbol.

# Configuración inicial
## Configuración RVM & Ruby

Instalar rvm

    curl -L https://get.rvm.io | bash -s stable
    rvm notes # ensure it worked

Instalar tu versión de Ruby

    rvm list known
    rvm install 2.5.2

Configure su `.rvmrc` para permitir la conmutación basada en proyectos

    # ~/.rvmrc
    # enable switching to default / system when leaving a directory
    rvm_project_rvmrc_default=1


## Preparando aplicación

Para que el servidor Rails se ejecute localmente:
- Clona este repo
`git clone https://robertolara@bitbucket.org/robertolara/resuelve_app.git`

- `gem install bundler` instalar la librería de bundler
- `bundle install --without production` para instalar todas las dependencias

Ejecute el conjunto de pruebas para verificar que todo funciona correctamente:

```
$ bundle exec rspec spec
```

Si las pruebas pasan, puede ejecutar la aplicación en el servidor local:

```
$ rails server
```
