require 'rails_helper'

module SalariesCalculator
  module Services

    describe GetPlayersWithStruct do
      let(:players_array) do
        JSON.parse('
          [  
            {  
                "nombre":"Juan Perez",
                "nivel":"C",
                "goles":10,
                "sueldo":50000,
                "bono":25000,
                "sueldo_completo":null,
                "equipo":"rojo"
            }
          ]')
      end

      let(:empty_players_array) do
        JSON.parse('[]')
      end

      describe "#call" do
        context "with players" do
          it "return a list of player instance" do
            service = GetPlayersWithStruct.new(players_array)

            expect(service.call.count).to eq(1)
          end
        end

        context "with empty list of players" do
          it "return a empty list" do
            service = GetPlayersWithStruct.new(empty_players_array)

            expect(service.call.count).to eq(0)
          end
        end
      end
    end
  end
end