require 'rails_helper'

module SalariesCalculator
  describe ForPlayers do
    before(:each) do
      @players_hash = JSON.parse('
        {
          "jugadores": [  
            {  
                "nombre":"Juan Perez",
                "nivel":"C",
                "goles":10,
                "sueldo":50000,
                "bono":25000,
                "sueldo_completo":null,
                "equipo":"rojo"
            }
          ]
        }')
    end
    
    describe "#calculate" do
      context "with empty players list" do
        it "returns empty array" do
          expected_result = []
          service_players = ForPlayers.new({}).calculate
          expect(service_players).to eq(expected_result)
        end
      end

      context "with missing attributes for players" do
        let(:players_hash) { 
          JSON.parse('
            {
              "jugadores": [  
                {  
                    "nombre":"Juan Perez",
                    "goles":10,
                    "sueldo":50000,
                    "bono":25000,
                    "sueldo_completo":null,
                    "equipo":"rojo"
                }
              ]
            }')
         }

        it "returns empty array" do
          expected_result = []
          service_players = ForPlayers.new(players_hash).calculate
          expect(service_players).to eq(expected_result)
        end
      end


      context "with full data of players" do
        it "returns players array with salaries" do
          expected_result = [  
              {  
                  nombre: "Juan Perez",
                  nivel: "C",
                  goles: 10,
                  sueldo: 50000,
                  bono: 25000,
                  sueldo_completo: 66666.66,
                  equipo: "rojo"
              }
            ]

          service_players = ForPlayers.new(@players_hash.with_indifferent_access).calculate
          expect(service_players.first.full_salary).to eq(expected_result[0][:sueldo_completo])
        end
      end   
    end
  end
end
