require 'rails_helper'

RSpec.describe "Players api", type: :request do
   # Test suite for POST /players/salaries
   describe 'POST /players/salaries' do
    # valid payload
    let(:valid_attributes) do
      JSON.parse('
      {
        "jugadores": [  
          {  
              "nombre":"Juan Perez",
              "nivel":"C",
              "goles":10,
              "sueldo":50000,
              "bono":25000,
              "sueldo_completo":null,
              "equipo":"rojo"
          },
          {  
              "nombre":"EL Cuauh",
              "nivel":"Cuauh",
              "goles":30,
              "sueldo":100000,
              "bono":30000,
              "sueldo_completo":null,
              "equipo":"azul"
          },
          {  
              "nombre":"Cosme Fulanito",
              "nivel":"A",
              "goles":7,
              "sueldo":20000,
              "bono":10000,
              "sueldo_completo":null,
              "equipo":"azul"
    
          },
          {  
              "nombre":"El Rulo",
              "nivel":"B",
              "goles":9,
              "sueldo":30000,
              "bono":15000,
              "sueldo_completo":null,
              "equipo":"rojo"
    
          }
        ]
      }')
    end

    context 'when the request is valid' do
      before { post '/api/v1/players/salaries', params: valid_attributes }

      it 'calculate salaries' do
        expect(json['jugadores'].count).to eq(4)
        expect(json['jugadores'].first['nombre']).to eq("Juan Perez")
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the request is invalid' do
      before { post '/api/v1/players/salaries', params: { } }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body)
          .to match("{\"jugadores\":[]}")
      end
    end
  end
end
